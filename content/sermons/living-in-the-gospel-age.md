---
title: "Living in th Gospel Age"
date: 2018-02-04
author: Rev. Crawford
description : "1 Peter 1:10-12"
---

{{< sermon LivingInTheGospelAge >}}

This sermon is based upon 1 Peter 1:10-12 and was preached by Rev. Crawford on January 28, 2018.

You can also download the audio file [here](https://archive.org/download/LivingInTheGospelAge/Living%20In%20the%20Gospel%20Age.mp3).

