---
title: "Service Not Speculation"
date: 2020-09-06
author: Rev. Crawford
description : "1 Thessalonians 5:1-11"
---

{{< sermon service-not-speculation >}}

This sermon is based upon 1 Thessalonians 5:1-11 and was preached by Rev. Crawford on September 6, 2020.
