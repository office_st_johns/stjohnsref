---
title: "A Pleasing Life"
date: 2020-08-09
author: Rev. Crawford
description : "1 Thessalonians 2:1-12"
---

{{< sermon apleasing-life >}}

This sermon is based upon 1 Thessalonians 2:1-12 and was preached by Rev. Crawford on August 9, 2020.
