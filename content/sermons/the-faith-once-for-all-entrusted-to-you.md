---
title: "The Faith Once For All Entrusted to You"
date: 2018-10-07
author: Rev. Crawford
description : "Jude 3-4"
---

{{< sermon Jude34 >}}

This sermon is based upon Jude 5-16 and was preached by Rev. Crawford on October 7, 2018.

You can also download the audio file [here](https://archive.org/download/Jude34/Jude%203-4.mp3).

