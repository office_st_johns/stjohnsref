---
title: "Self Examination"
date: 2018-10-14
author: Rev. Crawford
description : "Jude 5-16"
---

{{< sermon Jude516October14 >}}

This sermon is based upon Jude 5-16 and was preached by Rev. Crawford on October 14, 2018.

You can also download the audio file [here](https://archive.org/download/Jude516October14/Jude%205-16%20October%2014.mp3).

