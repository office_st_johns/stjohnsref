---
title: "Living as Stewards of the Shepherd"
date: 2020-09-27
author: Rev. Crawford
description : "Micah 3-5"
---

{{< sermon living-as-stewards-of-the-shepherd >}}

This sermon is based upon Micah 3-5 and was preached by Rev. Crawford on September 27, 2020.

