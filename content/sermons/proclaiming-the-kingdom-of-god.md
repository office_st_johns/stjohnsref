---
title: "Proclaiming the Kingdom of God"
date: 2018-12-30
author: Rev. Crawford
description : "Genesis 11:27-12:9"
---

{{< sermon Dec30Gen1127129 >}}

This sermon is based upon Genesis 11:27-12:9 and was preached by Rev. Crawford on December 30, 2018.

You can also download the audio file [here](https://archive.org/download/Dec30Gen1127129/Dec%2030%20Gen%2011%2027-12%209.mp3).

