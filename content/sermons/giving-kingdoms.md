---
title: "Giving Kingdoms"
date: 2018-04-05
author: Rev. Crawford
description : "Daniel 2"
---

{{< sermon Daniel2_201805 >}}

This sermon is based upon Daniel 2 and was preached by Rev. Crawford on April 05, 2018.

You can also download the audio file [here](https://archive.org/download/Daniel2_201805/Daniel_2.mp3).

