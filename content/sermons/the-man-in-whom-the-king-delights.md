---
title: "The Man In Whom the King Delights to Honor"
date: 2018-05-27
author: Rev. Crawford
description : "Esther 3:1-6; 5:8-6:10"
---

{{< sermon TheManInWhomTheKingDelightsToHonor >}}

This sermon is based upon Esther 3:1-6; 5:8-6:10 and was preached by Rev. Crawford on May 27, 2018.

You can also download the audio file [here](https://archive.org/download/TheManInWhomTheKingDelightsToHonor/The%20Man%20In%20Whom%20the%20King%20Delights%20to%20Honor.mp3).

