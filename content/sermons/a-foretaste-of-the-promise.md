---
title: "A Foretaste of the Promise"
date: 2019-01-23
author: Rev. Crawford
description : "Genesis 23"
---

{{< sermon jan27gen23 >}}

This sermon is based upon Genesis 23 and was preached by Rev. Crawford on January 23, 2019.

You can also download the audio file [here](https://archive.org/download/jan27gen23/Jan%2027%20Gen%2023.mp3).

