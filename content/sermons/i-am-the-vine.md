---
title: "I Am the Vine"
date: 2020-03-21
author: Rev. Crawford
description : "John 15:1-17"
---

{{< youtube PVj9kHJysyw >}}

This sermon is based upon John 15:1-17 and was preached by Rev. Crawford on March 22, 2020.
