---
title: "God Is Present in the Midst of the Storm"
date: 2017-09-17
author: Rev. Crawford
description : "Acts 27:13-34"
---

{{< sermon GodPresentInStorms >}}

This sermon is based upon Acts 27:13-34 and was preached by Rev. Crawford on September 17, 2017.

You can also download the audio file [here](https://archive.org/download/GodPresentInStorms/God_Present_in_Storms.mp3).

