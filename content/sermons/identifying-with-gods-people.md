---
title: "Identifying with God's People"
date: 2018-06-03
author: Rev. Crawford
description : "Esther 4:5-17"
---

{{< sermon ToIdentifyWithGodsPeople >}}


This sermon is based upon Esther 4:5-17 and was preached by Rev. Crawford on June 3, 2018.

You can also download the audio file [here](https://archive.org/download/ToIdentifyWithGodsPeople/To%20Identify%20With%20God%27s%20People.mp3).

