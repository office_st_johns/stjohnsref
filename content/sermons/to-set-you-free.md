---
title: "To Set You Free"
date: 2017-12-24
author: Rev. Crawford
description : "John 8:31-59"
---

{{< sermon ToSetYouFree >}}

This sermon is based upon John 8:31-59 and was preached by Rev. Crawford on December 24, 2017.

You can also download the audio file [here](https://archive.org/download/ToSetYouFree/To_Set_You_Free.mp3).

