---
title: "When Adversity Arises"
date: 2020-05-24
author: Rev. Crawford
description : "Exodus 5-6"
---

{{< sermon when-adversity-arises >}}

This sermon is based upon Exodus 5:1-6:13 and was preached by Rev. Crawford on May 24, 2020.
