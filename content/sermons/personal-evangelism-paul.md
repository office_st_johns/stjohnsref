---
title: "Personal Evangelism: Paul and the King"
date: 2017-09-10
author: Rev. Crawford
description : "Acts 26"
---

{{< sermon PersonalEvangelism >}}

This sermon is based upon Acts 26 and was preached by Rev. Crawford on September 10, 2017.

You can also download the audio file [here](https://archive.org/download/PersonalEvangelism/Personal%20Evangelism.mp3).

