---
title: "God Provides"
date: 2019-01-20
author: Rev. Crawford
description : "Genesis 22"
---

{{< sermon jan20gen22 >}}

This sermon is based upon Genesis 22 and was preached by Rev. Crawford on January 20, 2019.

You can also download the audio file [here](https://archive.org/download/jan20gen22/Jan%2020%20Gen%2022.mp3).

