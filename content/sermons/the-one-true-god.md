---
title: "The One True God"
date: 2017-10-22
author: Rev. Crawford
description : "Isaiah 44"
---

{{< sermon OneTrueGod_201710 >}}

This sermon is based upon Isaiah 44 and was preached by Rev. Crawford on October 22, 2017.

You can also download the audio file [here](https://archive.org/download/OneTrueGod_201710/OneTrueGod.mp3).

