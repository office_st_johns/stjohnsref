---
title: "The God Who Is"
date: 2020-05-17
author: Rev. Crawford
description : "Exodus 3-4"
---

{{< sermon calledand-equipped >}}

This sermon is based upon Exodus 3-4 and was preached by Rev. Crawford on May 17, 2020.
