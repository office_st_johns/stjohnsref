---
title: "Deposing Rulers"
date: 2018-05-06
author: Rev. Crawford
description : "Daniel 5"
---

{{< sermon Daniel5_201805 >}}

This sermon is based upon Daniel 5 and was preached by Rev. Crawford on May 06, 2018.

You can also download the audio file [here](https://archive.org/download/Daniel5_201805/Daniel%205.mp3).

