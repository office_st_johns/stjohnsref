---
title: "God's Justice"
date: 2017-11-05
author: Rev. Crawford
description : "Habakkuk"
---

{{< sermon GodsJustice >}}

This sermon is based upon the book of Habakkuk and was preached by Rev. Crawford on November 05, 2017.

You can also download the audio file [here](https://archive.org/download/GodsJustice/God%27s%20Justice.mp3).

