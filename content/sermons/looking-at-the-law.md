---
title: "Looking at the Law"
date: 2020-06-28
author: Rev. Crawford
description : "Exodus 19"
---

{{< sermon looking-at-the-law >}}

This sermon is based upon Exodus 18 and was preached by Rev. Crawford on June 28, 2020.
