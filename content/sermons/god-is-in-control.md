---
title: "God Is In Control"
date: 2017-10-29
author: Rev. Crawford
description : "Daniel 4:28-37"
---

{{< sermon GodisinControl_201710 >}}

This sermon is based upon Daniel 4:28-37 and was preached by Rev. Crawford on October 29, 2017.

You can also download the audio file [here](https://archive.org/download/GodisinControl_201710/GodisinControl.mp3).

