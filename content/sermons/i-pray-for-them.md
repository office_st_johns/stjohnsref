---
title: "I Pray for Them"
date: 2018-07-08
author: Rev. Crawford
description : "John 17:6-19"
---

{{< sermon IPrayForThemJohn17619 >}}

This sermon is based upon John 17:6-19 and was preached by Rev. Crawford on July 8, 2018.

You can also download the audio file [here](https://archive.org/download/IPrayForThemJohn17619/I_Pray_For_Them_John_17_6-19.mp3).

