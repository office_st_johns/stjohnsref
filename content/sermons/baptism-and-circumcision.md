---
title: "Baptism and Circumcision"
date: 2019-01-06
author: Rev. Crawford
description : "Genesis 17"
---

{{< sermon jan6gen17327 >}}

This sermon is based upon Genesis 17 and was preached by Rev. Crawford on January 06, 2019.

You can also download the audio file [here](https://archive.org/download/jan6gen17327/Jan%206%20Gen%2017%203%20-%2027.mp3).

