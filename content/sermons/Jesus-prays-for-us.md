---
title: "Jesus prays for his people"
date: 2020-04-04
author: Rev. Crawford
description : "John 17"
---

{{< youtube jWpRhMd0HdA >}}

This sermon is based upon John 17 and was preached by Rev. Crawford on April 5, 2020.



Article recommending Scripture memorization:

https://www.charadox.com/posts/why-memorize-scripture-when-you-can-google-it

Recommended Scriptures for memorization:

John 3:16-17; 1 Timothy 3:16; Galatians 5:22-23; 2 Corinthians 5:17; 1 Corinthians 5:21; 1 Thessalonians 5:16-18; Psalm 23; Matthew 28:18-20; Colossians 1:15-20
