---
title: "Sent for Your Benefit"
date: 2020-03-29
author: Rev. Crawford
description : "John 16"
---

{{< youtube V4ksOqjKoXo >}}

This sermon is based upon John 16 and was preached by Rev. Crawford on March 29, 2020.

Some scripture passages for additional reading and reflection:

John 11:25-26; Ephesians 1; Romans 1; 3; 15; Galatians 3; Psalm 103
