---
title: "That They May Be One"
date: 2018-07-15
author: Rev. Crawford
description : "John 17:20-26"
---

{{< sermon ThatTheyMayBeOneJohn172026 >}}

This sermon is based upon John 17:20-26 and was preached by Rev. Crawford on July 15, 2018.

You can also download the audio file [here](https://archive.org/download/ThatTheyMayBeOneJohn172026/That_They_May_Be_One_John_17_20-26.mp3).

