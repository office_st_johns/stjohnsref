---
title: "A Christian"
date: 2018-09-30
author: Rev. Crawford
description : "Jude 1-2"
---

{{< sermon Jude12 >}}

This sermon is based upon Jude 5-16 and was preached by Rev. Crawford on September 30, 2018.

You can also download the audio file [here](https://archive.org/download/Jude12/Jude%201-2.mp3).

