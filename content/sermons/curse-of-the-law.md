---
title: "The Curse of the Law"
date: 2024-06-27
author: Rev. Crawford
description: "Galatians 3:10-14"
---

This is the service link for Sunday, June 30, 2024. The sermon is based on Galatians 3:10-14.

{{< youtube GbvcNECrjoo>}}