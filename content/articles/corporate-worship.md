---
title: "Corporate Worship: Images of the Church"
date: 2015-10-08
author: Rev. Crawford
description : "Corporate Worship"
---

THE FAMILY OF GOD THE FATHER

The church is known as the family of faith (Gal. 6:9-10), the household of God (1 Tim. 3:14-15), and sons [and daughters - 'sons,' like 'men' is inclusive language in some contexts] (Gal. 4:4-6, Eph. 1:4-6, Rom. 8:12-17).  As such, we have many benefits as part of God's family.  The Westminster Confession of Faith has a great section outlining these benefits (WCF 12.1): "All those that are justified, God vouchsafeth [to give or grant something graciously], in and for His only Son Jesus Christ, to make partakers of the grace of adoption: by which they are taken into the number, and enjoy the liberties and privileges of the children of God; have His name put upon them, receive the Spirit of adoption; have access to the throne of grace with boldness; are enabled to cry, Abba, Father; are pitied, protected, provided for, and chastened by Him as by a Father; yet never cast off, but sealed to the day of redemption, and inherit the promises, as heirs of everlasting salvation."  These few sentences are packed with promises made by God to his children which we can take hold of in the midst of life's struggles.  To take one example, think about the idea of having "access to the throne of grace with boldness."  Jews in the Old Testament didn't have this bold access.  They were barred from the Holy of Holies but for once a year and had to approach with caution.  Through Christ, however, access has been opened to us as He sits at the right hand of God interceding on our behalf.  We can therefore approach with joy and confidence because our Mediator is present for us.  

THE BRIDE OF JESUS THE SON

Every time that we partake of communion, we are anticipating the great feast that we will one day have on high.  John of Patmos writes that this feast will be called the 'marriage supper of the Lamb' (Rev. 19:7-9).  Since we are the bride of Jesus, we need to keep in mind the reasons for his sacrifice on our behalf (Eph. 5:25-27): "Husbands, love your wives, as Christ loved the church and gave himself up for her, that he might sanctify her, having cleansed her by the washing of water with the word, so that he might present the church to himself in splendor, without spot or wrinkle or any such thing, that she might be holy and without blemish." 

THE TEMPLE OF GOD THE HOLY SPIRIT

The church only exists because of the Holy Spirit who gave the church life (Acts 2) and enables her to worship 'in spirit and truth' (John 4).  Jesus has promised to be present with his people when they gather in his name (Matt. 18), to never leave them or forsake them (Matt. 28), and to dwell in their midst (1 Cor. 3:16-17).  It is through the Holy Spirit that we come into the presence of the heavenly Jerusalem when we gather for corporate worship on Sunday mornings (Heb. 12).

These images make it clear that the people are God's temple, i.e., his house, and not the building.  This is another reason for us to grasp the reality of the presence of God when we gather on Sunday mornings.

