---
title: "Church Invisible"
date: 2015-10-14
author: Rev. Crawford
description : "Church Invisible"
---

INVISIBLE: unable to be seen; not visible to the eye.

In centuries past, things like bacteria, the amount of 'life' in a drop of water, and yeast plants were 'invisible.'  No one could see them or even knew of their existence.  It wasn't until the late 17th century that a man invented a microscope that allowed us 'to see.'  Apart from the microscope, these things are invisible to our eyes.  We know when we're affected by bacteria, but we can't see it being passed along to us.  We see water as transparent, not seeing all of the 'minerals added for taste.'  

Likewise, when we speak of the church, we can speak of her in a two-fold sense: visible and invisible.  The church visible is anyone who has professed faith in Christ, and their children, and joined themselves to a local church.  It is to such churches (i.e., visible churches) that we have our letters written in the New Testament (e.g., "to the church of God that is in Corinth," "to the saints in Ephesus," "to all the saints in Christ Jesus who are in Philippi").  The invisible church, however, is composed of those who are truly saved.  This distinction draws on the New Testament images such as the wheat and the tares.  They are allowed to grow along side one another until the final harvest.  In other words, there will be true believers as we well unbelievers within the church and her membership until the final day.  We can only see the visible church; it's there, but we don't have the eyes to see.  

As James Bannerman writes in The Church of Christ, "The form of the invisible Church cannot be distinguished by the eye of man, for the features and lineaments of it are known only to God; wheras the form of the visible Church is marked out and defined by its external government, ordinances, and arrangements.  The members of the invisible Chuch cannot be discerned or detected by the eye of man, for their call is the inward call of the Spirit, and their relation to Christ a spritual and unseen one; whereas the members of the visible Church stand revealed to the sight of all by the outward profession they make, and the external connection in which they stand to Christ, as they enjoy the privelegs and ordinances of His appointment."
