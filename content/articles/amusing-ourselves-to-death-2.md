---
title: "Amusing Ourselves to Death, Part 2"
date: 2016-09-02
author: Rev. Crawford
description : "Amusing Ourselves to Death"
---

Amusing Ourselves to Death by Neil Postman

You can view the first part of this post here.

We often talk today about having an information overload.  This is particularly true if you're on the web.  Just look at the number of sources that you can reach to get your 'news' for the day: MSN, Google, Yahoo, Wikipedia, Facebook, etc.  The list goes on and on.  Not only that, but if you have anything you wish to learn about you can always look it up on YouTube.  The possibilities, it seems, are endless.  This is also particularly relevant as we watch discourse within social media.  Everyone, it seems, has an answer and opinion on any political or religious or _____ (fill in the blank) issue.  Again, this provocative little work gets interesting at this point, as well.  

I do not wish to simply copy and paste much of what he writes, but this section is particularly jolting, so I’ll produce it verbatim:

“you may get a sense of what is meant by context-free information by asking yourself the following question: How often does it occur that information provided you on morning radio or television [insert media], or in the morning newspaper, causes you to alter your plans for the day, or to take some action you would not otherwise have taken, or provides insight into some problem you are required to solve?  For most of us, news of the weather will sometimes have such consequences; for investors, news of the stock market; perhaps an occasional story about a crime will do it, if by chance the crime occurred near where you live or involved someone you know.  But most of our daily news is inert, consisting of information that gives us something to talk about but cannot lead to any meaningful action.  This fact is the principal legacy of the telegraph: By generating an abundance of irrelevant information, it dramatically altered what may be called the “information-action ratio.”

“In both oral and typographic cultures, information derives its importance from the possibilities of action.  Of course, in any communication environment, input…always exceeds output.  But the situation created…by later technologies, made the relationship between information and action both abstract and remote.  For the first time in human history, people were faced with the problem of information glut, which means that simultaneously they were faced with the problem of a diminished social and political potency. 

“You may get a sense of what this means by asking yourself another series of questions: What steps do you plan to take to reduce the conflict in the Middle East?  Or the rates of inflation, crime, and unemployment?  What are your plans for preserving the environment or reducing the risk of nuclear war?  What do you plan to do about NATO…the CIA…?  I shall take liberty of answering for you: You plan to do nothing about them.  You may, of course, cast a ballot for someone who claims to have some plans, as well as the power to act.  But this you can do only once every two or four years by giving one hour of your time, hardly a satisfying means of expressing the broad range of opinions you hold.  Voting, we might even say, is the next to last refuge of the politically impotent.  The last refuge, of course, is giving your opinion to a pollster, who will get a version of it through a desiccated question, and then submerge it in a Niagara of similar opinions, and convert them into – what else? – another piece of news.  Thus, we have here a great loop of impotence: The news elicits from you a variety of opinions about which you can do nothing except to offer them as more news, about which you can do nothing” (pages 68-69).
