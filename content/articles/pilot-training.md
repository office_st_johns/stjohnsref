---
title: "Pilot Training"
date: 2015-08-14
author: Rev. Crawford
description : "Pilot Training"
---

TRAINING: the action of teaching (a person or animal) a particular skill or type of behavior through practice and instruction over a period of time

While I was working as a shift manager for Pilot Travel Centers, I always had fun trying to tell people what I did for a living.  
"So where do you work?"
"I work at Pilot."  
"You're a pilot?!"  
...
"Not quite...I work at a Travel Center...okay, it's a truck stop..."

Sooner or later, people would get the picture, and I found myself slowly hanging my head with a bit of shame since it was such a far cry from their vision.  Over time, however, I became used to the plummet of self-esteem and tried to see my job in a positive light.  I failed miserably.  It wasn't the fact that I didn't like my job or that I found it difficult or stressful.  The problem for me was trying to understand the purpose in my being there.  Here I was, with my college and seminary degrees, working at a truck stop.  All of my time (and money) spent on those years of education felt wasted.  It wasn't until someone spoke truth into my life and helped me reorient myself that I was able to see the light (if only dimly).  

He offered me reassurance and encouragement that God had me right where he wanted me to be.  He had chosen to place me at Pilot as a training ground for what was to come.  Looking back, the dim light now has a brightness rivaling the sun, and I can see the truth of these words.  There were some essential skills that I gained from working at Pilot: P&L statements for budget reading and understanding, managing others and assigning tasks as a part of leadership, and developing daily plans to meet and accomplish goals.  These are just a few of the things that I would not have learned had I not had my experience at Pilot.  It does not mean that it made the time there any easier or shorter.  It does mean, however, that there was a purpose to my being there.  Despite the hardships and challenges, God designed it as a time of growth for me.  

So let me encourage you wherever you may find yourselves today.  God has you right where you need to be.  Times may be hard and struggles may be had, but God will use the circumstances in your life as a way for you to grow.  You may be gaining skills that you will use later in life or you may be shaped in essential aspects of your character.  No matter what you're facing, remember that God loves his people and walks with you every step of the way.  How will you react?  Will you seek to cultivate the hope which does not disappoint (Romans 5:3ff) or will you follow the world into the abyss of hopelessness?  
