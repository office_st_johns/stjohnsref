---
title: "D.A. Carson on Worship"
date: 2015-08-18
author: Rev. Crawford
description : "D.A. Carson on Worship"
---

This is from Carson's opening chapter in Worship by the Book.  I'll let his words speak for themselves:

“In an age increasingly suspicious of (linear) thought, there is much more respect for the “feelings” of things – whether a film or a church service. It is disturbingly easy to plot surveys of people, especially young people, drifting from a church of excellent preaching and teaching to one with excellent music because, it is alleged, there is “better worship” there. But we need to think carefully about this matter. Let us restrict ourselves for the moment to corporate worship. Although there are things that can be done to enhance corporate worship, there is a profound sense in which excellent worship cannot be attained merely by pursuing excellent worship. In the same way that, according to Jesus, you cannot find yourself until you lose yourself, so also you cannot find excellent corporate worship until you stop trying to find excellent corporate worship and pursue God himself. Despite the protestations, one sometimes wonders if we are beginning to worship worship rather than worship God. As a brother put it to me, it’s a bit like those who begin by admiring the sunset and soon begin to admire themselves admiring the sunset.

his point is acknowledged in a praise chorus like “Let’s forget about ourselves, and magnify the Lord, and worship him.” The trouble is that after you have sung this repetitious chorus three or four times, you are no farther ahead. The way you forget about yourself is by focusing on God—not by singing about doing it, but by doing it. There are far too choruses and services and sermons that expand our vision of God—his attributes, his works, his character, his words. Some think that corporate worship is good because it is lively where it had been dull. But it may also be shallow where it is lively, leaving people dissatisfied and restless in a few months’ time. Sheep lie down when they are well fed (cf. Psalm 23:2); they are more likely to be restless when they are hungry. “Feed my sheep,” Jesus commanded Peter (John 21); and many sheep are unfed. If you wish to deepen the worship of the people of God, above all deepen their grasp of his ineffable majesty in his person and in all his works.

We do not expect the garage mechanic to expatiate on the wonders of his tools; we expect him to fix the car. He must know how to use his tools, but he must not lose sight of the goal. So we dare not focus on the mechanics of corporate worship and lose sight of the goal. We focus on God himself, and thus we become more godly and learn to worship—and collaterally we learn to edify one another, forbear with one another, challenge one another.”

