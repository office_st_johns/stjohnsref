---
title: "The Christian Anomaly"
date: 2015-08-13
author: Rev. Crawford
description : "The Christian Anomaly"
---

ANOMALY: something that deviates from what is standard, normal, or expected

Bannerman, in Church of Christ, writes: "A solitary Christian is worse than a contradiction, he is an anomaly, standing out against the express institution of God, which has appointed the fellowship of believers in one Church, and made provision in its outward ordinances for their union and edification.  The Christian society is a kingdom, set up by express Divine appointment, and differs from every other society on earth in this remarkable fact, that the builder and maker of it is God."  

Upon reading this, I remember how there was a time in my life when I would have been seen as an anomaly.  It certainly wouldn't have appeared that way to anyone else since I was attending a Christian school and was surrounded by Christian brethren but for several seasons in my early college days, I spent Sunday mornings in my dorm room.  I would argue that I tried to make it to chapel most weeks and so that should make it okay, but looking back I realize how much time I missed being connected to a body of believers.  I heard words of exhortation about the importance of being connected to a local body, but they were falling on deaf ears at the time.  Even this lofty language of Bannerman would probably have passed over me quick as a breeze, never having time to settle on my heart and convince me of my need.  

Now, however, I can hear these words as refreshing and encouraging.  It is indeed remarkable that we have God as our "builder and maker."  The church does not exist by human appointment or plan.  Jesus himself established this kingdom, and he promises that it shall never cease (Matthew 16:18, And I tell you that you are Peter, and on this rock I will build my church, and the gates of Hades will not overpower it).  True, there are churches that water down the Gospel or which are little more than a social club, but the church of Christ is characterized by so much more.  There is a hope and grace offered through Jesus that cannot be experienced or grasped outside of Himself.  Churches which just offer messages of "be happy and well" are robbing believers of being able to experience the gracious mercy in Jesus and the offer of forgiveness for their sins.  In essence, they are missing out on the reality of their relationship with God - that he is not only their Creator but also Redeemer.  

So the next time that you're thinking about Sunday morning (or other times of Christian fellowship), remember who the builder and maker of the Church is.  It is not dependent upon the beauty of the structure, the skill of the preacher, or the number of people in attendance.  Its establishment rests on the proclamation of Jesus and his promise to keep it safe until the end.
