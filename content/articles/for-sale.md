---
title: "For Sale"
date: 2015-08-17
author: Rev. Crawford
description : "For Sale"
---

SALE: the exchange of a commodity for money; the action of selling something

As I was driving today, I couldn't help but notice the number of "for sale" signs posted between my house and church.  There's the house across the street, the two cars in neighborhood lawns, and the truck across from Sheetz, all within a mile.  On top of that, there was yard sale upon yard sale this summer with signs directing you up one street and down another.  Everywhere we go, it seems like people are selling something.  You go to the teller at the bank, and they're trying to convince you to sign up for the newest version of their credit card.  You order something from a restaurant, and they do everything they can to 'up sell' your order.  Employees are trained in asking the right questions and making the right gestures to convince you to get that foot long instead of the six-inch or to make your sandwich into a meal.  You take your car to the garage for that pesky check engine light, and they try to get you for an oil change, a radiator flush, and a replacement of your spark plugs.

As we pay for all of these goods and services, it's no wonder people come to the conclusion that nothing is free.  In a television show I watched recently, one of the characters was upset that someone had purchased them a Christmas present.  The expectation in gift giving, he said, is that one will receive something in return, so once someone receives a gift, they feel obligated to offer one back.  It's an exchange of gifts, after all.  Likewise, many 'free' preferred customer cards from different companies require you to sign up with your email address before you get their 'free discounts.'  You'll get the discount at the cost of your inbox (and possibly sanity...how many times do you have to check the box and then click 'delete' in a day?  Alternatively, you could be like some people I know and just have 900+ unread emails...).  

It should be no surprise, then, that people have this approach to the Gospel.  They set out trying to buy their way into the kingdom.  Perhaps if I give enough money to the church or spend enough hours volunteering at the hospital, then I will pay for my place in heaven.  It must cost me something, after all, everything else in life does.  When they hear the message that Jesus paid the price and that salvation is offered freely without cost, by grace, their minds can't comprehend the idea.  There must be something that I have to do.  What's the catch?  Where's the fine print?  It's a gift, offered outside of the Christmas gift exchange mentality, away from the up-sell strategy, free from the constant "for sale" signs posted around us.  Jesus really did pay it all, and there's nothing that we can add to his perfect work.  Praise God.

