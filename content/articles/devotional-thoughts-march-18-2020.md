---
title: "Devotional Thoughts on March 18, 2020"
date: 2020-03-18
author: Rev. Crawford
description : "Wednesday, March 18 Devotional Thoughts"
---

Some prayers and readings taken from the Book of Common Prayer

Habakkuk 2:18-20:
“What profit is an idol
    when its maker has shaped it,
    a metal image, a teacher of lies?
For its maker trusts in his own creation
    when he makes speechless idols!
Woe to him who says to a wooden thing, Awake;
    to a silent stone, Arise!
Can this teach?
Behold, it is overlaid with gold and silver,
    and there is no breath at all in it.
But the Lord is in his holy temple;
    let all the earth keep silence before him.”

Prayer of Confession

Almighty and most merciful Father, we have erred and strayed from thy ways like lost sheep.  We have followed too much the devices and desires of our own hearts.  We have offended against thy holy laws.  We have left undone those things which we ought to have done; and we have done those things which we ought not to have done; and there is no health in us.  But thou, O Lord, have mercy upon us, miserable offenders.  Spare thou those, O God, who confess their faults.  Restore thou those who are penitent; according to thy promises declared unto mankind in Christ Jesus our Lord.  And grant, O most merciful Father, for his sake; that we may hereafter live a godly, righteous, and sober life, to the glory of thy holy name.  Amen.

Psalm 103:1-14

Bless the Lord, O my soul,
    and all that is within me,
    bless his holy name!
Bless the Lord, O my soul,
    and forget not all his benefits,
who forgives all your iniquity,
    who heals all your diseases,
who redeems your life from the pit,
    who crowns you with steadfast love and mercy,
who satisfies you with good
    so that your youth is renewed like the eagle's.

The Lord works righteousness
    and justice for all who are oppressed.
He made known his ways to Moses,
    his acts to the people of Israel.
The Lord is merciful and gracious,
    slow to anger and abounding in steadfast love.
He will not always chide,
    nor will he keep his anger forever.
He does not deal with us according to our sins,
    nor repay us according to our iniquities.
For as high as the heavens are above the earth,
    so great is his steadfast love toward those who fear him;
as far as the east is from the west,
    so far does he remove our transgressions from us.
As a father shows compassion to his children,
    so the Lord shows compassion to those who fear him.
For he knows our frame;
    he remembers that we are dust.

Pray the Lord's Prayer

Psalm 51:15, "O Lord, open my lips,
    and my mouth will declare your praise."

{{< youtube c9zHn4QSH-8  >}}

Benedictus es, Domine

Blessed art thou, O Lord God of our fathers; praised and exalted above all forever
Blessed art thou for the Name of thy Majesty; praised and exalted above all forever.
Blessed art thou in the temple of thy holiness; praised and exalted above all forever.
Blessed art thou that beholdest the depths, and dwellest between the Cherubim; praised and exalted above all forever.
Blessed art thou on the glorious throne of thy kingdom; praised and exalted above all forever.
Blessed art thou in the firmament of heaven; praised and exalted above all forever.
Glory be to the Father and to the Son, and to the Holy Ghost;.
As it was in the beginning, is now and ever shall be, world without end. Amen.

Philippians 4:4-13

Rejoice in the Lord always; again I will say, rejoice. Let your reasonableness be known to everyone. The Lord is at hand; do not be anxious about anything, but in everything by prayer and supplication with thanksgiving let your requests be made known to God. And the peace of God, which surpasses all understanding, will guard your hearts and your minds in Christ Jesus.

Finally, brothers, whatever is true, whatever is honorable, whatever is just, whatever is pure, whatever is lovely, whatever is commendable, if there is any excellence, if there is anything worthy of praise, think about these things. What you have learned and received and heard and seen in me—practice these things, and the God of peace will be with you.

I rejoiced in the Lord greatly that now at length you have revived your concern for me. You were indeed concerned for me, but you had no opportunity. Not that I am speaking of being in need, for I have learned in whatever situation I am to be content. I know how to be brought low, and I know how to abound. In any and every circumstance, I have learned the secret of facing plenty and hunger, abundance and need. I can do all things through him who strengthens me.

A handful of appropriate reminders come through in this passage for us today.

1. Rejoice, always...always, rejoice. It can be hard for us to fathom how we can rejoice in midst of our present circumstances. How can we rejoice with the world-wide heartache? How can we rejoice when we're stuck in distance from one another? How can we rejoice when we run out of TP?? The key, I think, is tied with Paul's final words in this passage: no matter what he faced in life, he was able to get to the point where he was content in all things. Paul, the man who experienced shipwreck, stonings, beatings, derision, etc., was able to say, "I'm content, no matter my present-day circumstance. Notice that he says that he didn't get there on his own, he only got there because, "I can do all things through him who strengthens me." We often hear that verse applied to all kinds of situations, but Paul's point is that he can endure whatever hardships that he may face BECAUSE of the strength of Christ. We have to recognize our need and then look to the strength of Christ. Once we're able to be content no matter our circumstances, we can then look to rejoicing, no matter what we're facing.

2. Pray. I cannot express this enough, and neither could Paul. We ought to always be in prayer, but especially in the midst of this hardship. Prayer is the way that we do battle against such difficulties as we face today. Pray for the nations of the world, for healthcare workers and first responders. Pray for your neighbors and church family. Pray for world leaders. Pray for the scientists who are working to come up with treatments. Most of all, pray for the Lord's mercy to be poured out on this world. And use this as a time when your personal prayers are lifted up to God, knowing that you are secure if you are in Christ.
    
3. Spend time thinking on higher things than the present day sufferings. I'm not talking about living 'with your head in the clouds' so that 'you're no earthly good.' I'm saying that we can use a time when we're being forced to slow down as a means by which we take time to know God better. You've probably seen these words of C.S. Lewis floating around, but I'll also quote them here: "This is the first point to be made: and the first action to be taken is to pull ourselves together. If we are all going to be destroyed by an atomic bomb, let that bomb when it comes find us doing sensible and human things—praying, working, teaching, reading, listening to music, bathing the children, playing tennis, chatting to our friends over a pint and a game of darts—not huddled together like frightened sheep and thinking about bombs. They may break our bodies (a microbe can do that) but they need not dominate our minds."

Prayer:

ALMIGHTY and everlasting God, who dost govern all things in heaven and earth; Mercifully hear the supplications of thy people, and grant us thy peace all the days of our life.

O God, who art the author of peace and lover of concord, in knowledge of whom standeth our eternal life, whose service is perfect freedom; Defend us they humble servants in all assaults of our enemies; that we, surely trusting in thy defence, may not fear the power of any adversaries, through the might of Jesus Christ our Lord.

O Lord, our heavenly Father, Almighty and everlasting God, who hast safely brought us to the beginning of this day; Defend us in the same with they mighty power; that all our doings may be ordered by thy governance, to do always that is righteous in thy sight.
