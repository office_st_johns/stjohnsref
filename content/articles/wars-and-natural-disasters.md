---
title: "Wars and Natural Disasters"
date: 2015-10-20
author: Rev. Crawford
description : "Wars and Natural Disasters"
---

    In his book, How Long, O Lord, Reflections on Suffering and Evil, D.A. Carson touches upon social evils, poverty of different kinds, and war and natural disasters.  When we think about such things, we often turn and ask ‘why’ such things exist.  We don’t, however, see this particular question raised anywhere in the Bible.  God is not shocked, and neither should we be, by the presence of wars and natural disasters, “You will hear of wars and rumors of wars. Make sure that you are not alarmed, for this must happen, but the end is still to come.  For nation will rise up in arms against nation, and kingdom against kingdom. And there will be famines and earthquakes in various places” (Mattew 24:6-7).  This is not to say that these are good things, but the truth of the matter is that we should expect such things to exist in a fallen world (Carson, 60-61). 

     How should we respond to hurricanes, famines, tornadoes, tsunamis, and conflict?  Jesus offers insight, “Now there were some present on that occasion who told him about the Galileans whose blood Pilate had mixed with their sacrifices. He answered them, "Do you think these Galileans were worse sinners than all the other Galileans, because they suffered these things? No, I tell you! But unless you repent, you will all perish as well! Or those eighteen who were killed when the tower in Siloam fell on them, do you think they were worse offenders than all the others who live in Jerusalem? No, I tell you! But unless you repent you will all perish as well!” Carson draws three points from this passage (Carson, page 61):
     1. All death is a result of sin, and so we all deserve death (because we’re all sinners).
     2. Those who suffer is no evidence that they are more wicked than others.
     3. Jesus doesn’t treat these things as signs of the mysterious workings of God but as an incentive to repent.

     A quotes from Carson here is worthwhile, “We think we deserve times of blessing and prosperity, and that the times of war and disaster are not only unfair but come perilously close to calling into question God’s goodness or his power – even, perhaps, his very existence.  Jesus simply does not see it that way.  If we are to adopt his mind, we have some fundamental realignments to make in our assessment of ourselves” (Carson, 62).

