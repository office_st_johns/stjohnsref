---
title: "Devotional Thoughts on March 24, 2020"
date: 2020-03-24
author: Rev. Crawford
description : "Tuesday, March 24 Devotional Thoughts"
---

(Some prayers and readings taken from the Book of Common Prayer)

Psalm 19

The heavens declare the glory of God;
    the skies proclaim the work of his hands.
Day after day they pour forth speech;
    night after night they reveal knowledge.
They have no speech, they use no words;
    no sound is heard from them.
Yet their voice goes out into all the earth,
    their words to the ends of the world.
In the heavens God has pitched a tent for the sun.
    It is like a bridegroom coming out of his chamber,
    like a champion rejoicing to run his course.
It rises at one end of the heavens
    and makes its circuit to the other;
    nothing is deprived of its warmth.
The law of the LORD is perfect,
    refreshing the soul.
The statutes of the LORD are trustworthy,
    making wise the simple.
The precepts of the LORD are right,
    giving joy to the heart.
The commands of the LORD are radiant,
    giving light to the eyes.
The fear of the LORD is pure,
    enduring forever.
The decrees of the LORD are firm,
    and all of them are righteous.
They are more precious than gold,
    than much pure gold;
they are sweeter than honey,
    than honey from the honeycomb.
By them your servant is warned;
    in keeping them there is great reward.
But who can discern their own errors?
    Forgive my hidden faults.
Keep your servant also from willful sins;
    may they not rule over me.
Then I will be blameless,
    innocent of great transgression.
May these words of my mouth and this meditation of my heart
    be pleasing in your sight,
    LORD, my Rock and my Redeemer.

Yet we know that our words and our thoughts are not always pleasing in the sight of God as they ought to be. We confess that truth to God.

So we pray: Almighty and most merciful Father. We have erred and strayed from thy ways like lost sheep. We have followed too much our own desires. We have broken your law. We have done those things which we shouldn’t have done, and we’ve left undone those things which we should have done. But O Lord, have mercy upon us. Spare us, O God, as we confess our sins. Restore us as we repent, according to your promises in Christ Jesus our Lord. And grant, O most merciful Father, for his sake, that we live a godly, righteous, and sober life, to the glory of thy holy Name. Amen.

Romans 5:1-11

Therefore, since we have been justified through faith, we have peace with God through our Lord Jesus Christ, through whom we have gained access by faith into this grace in which we now stand. And we boast in the hope of the glory of God. Not only so, but we also glory in our sufferings, because we know that suffering produces perseverance; perseverance, character; and character, hope. And hope does not put us to shame, because God’s love has been poured out into our hearts through the Holy Spirit, who has been given to us.
You see, at just the right time, when we were still powerless, Christ died for the ungodly. Very rarely will anyone die for a righteous person, though for a good person someone might possibly dare to die. But God demonstrates his own love for us in this: While we were still sinners, Christ died for us.
Since we have now been justified by his blood, how much more shall we be saved from God’s wrath through him! For if, while we were God’s enemies, we were reconciled to him through the death of his Son, how much more, having been reconciled, shall we be saved through his life! Not only is this so, but we also boast in God through our Lord Jesus Christ, through whom we have now received reconciliation.

Pray the Lord’s Prayer.

Psalm 51:15

O Lord, open my lips, and my mouth will declare your praise.

This is probably the best recording of this song that I could find. It’s the words of Psalm 43 (which I’ve pasted below the video).

{{< youtube 8c108z2tLQk >}}

Psalm 43:

Vindicate me, my God,
    and plead my cause
    against an unfaithful nation.
Rescue me from those who are
    deceitful and wicked.
You are God my stronghold.
    Why have you rejected me?
Why must I go about mourning,
    oppressed by the enemy?
Send me your light and your faithful care,
    let them lead me;
let them bring me to your holy mountain,
    to the place where you dwell.
Then I will go to the altar of God,
    to God, my joy and my delight.
I will praise you with the lyre,
    O God, my God.
Why, my soul, are you downcast?
    Why so disturbed within me?
Put your hope in God,
    for I will yet praise him,
    my Savior and my God.

Scripture Reading:

James 1:2-4

Count it all joy, my brothers, when you meet trials of various kinds, for you know that the testing of your faith produces steadfastness. And let steadfastness have its full effect, that you may be perfect and complete, lacking in nothing.

Romans 5:3-5

Not only that, but we rejoice in our sufferings, knowing that suffering produces endurance, and endurance produces character, and character produces hope, and hope does not put us to shame, because God's love has been poured into our hearts through the Holy Spirit who has been given to us.

Both of these passages point to the idea that we should find a way to rejoice in our trials/sufferings. At the surface, such an idea seems awfully absurd: why should we be thankful and even joyous for having ‘bad things’ happen to us? Have you ever found yourself saying, “Thank you, God, that I’ve got the flu.” OR “Thank you, O Lord, that I’m cut off from most of my fellow believers.” ??? The point that God wants us to understand is not that we should somehow think that these bad things are just an illusion, or that all bad things are actually good things. Paul’s (and James and God’s, for that matter) point is that we should rejoice in sufferings because of their end result.
Of course, this presupposes that we have this basic understanding (that sufferings has a positive end result). This is not an effort to minimize human suffering. There are some very terrible things that happen to human beings and things that we, as humans, do to one another. There are some people who suffer for the majority of their lives, and I don’t want to sound as if we’re attempting to make it seems as if that’s all ‘okay.’ 

That being said, the focus is on the idea that we can learn steadfastness, or endurance, through such sufferings and trials. What we’re all going through right now, this quarantine, social distancing, or whatever you want to call it, is something that God can use to increase our endurance. It should be clear that we’re not talking about physical endurance right now (like our social distancing is somehow making our bodies stronger). This is endurance in a spiritual sense: we’re being strengthened, so long as, during this time we are leaning more into our God with trust and dependence. It’s one thing in the midst of this to think that the world is falling apart. It’s another to think that the world looks to be falling apart but that God is still in control. Moments such as these can bring us to a place where we are trusting in God even when we don’t know the full outcome of what is happening today.

Such work on our part (e.g., working on increasing our trust in God, going to him in prayer more regularly, calling one another to stay connected, and spending time in his Word) can also have a lasting impact: it can increase our character. Character is formed through our habits, and habits are formed through daily choice. What choices are you making in the midst of our current trial? How are you spending your days? What are you thinking about? How are you reacting to the current struggles?

If we respond as God directs, then we can trust that in the end, suffering will result in growing our hope. Recognition that such sufferings are not the end, or the ultimate goal of human life. God assures us elsewhere in his word that we will ultimately see any/all of our sufferings as a small portion of our existence in the long run, even though they don’t seem that way in the present. This is how we can be directed to rejoice in the midst of our sufferings.

Prayers:

O LORD, we beseech thee mercifully to receive the prayers of thy people which call upon thee; and grant that they may both perceive and know what things they ought to do, and also may have grace and power faithfully to fulfil the same; through Jesus Christ our Lord. Amen.

O God, who art the author of peace and lover of concord, in knowledge of whom standeth our eternal life, whose service is perfect freedom; Defend us they humble servants in all assaults of our enemies; that we, surely trusting in thy defence, may not fear the power of any adversaries, through the might of Jesus Christ our Lord. Amen.

O Lord, our heavenly Father, Almighty and everlasting God, who hast safely brought us to the beginning of this day; Defend us in the same with they mighty power; that all our doings may be ordered by thy governance, to do always that is righteous in thy sight; through Jesus Christ our Lord. Amen.

