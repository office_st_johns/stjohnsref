---
title: "Bedtime Struggles"
date: 2015-08-16
author: Rev. Crawford
description : "Bedtime Struggles"
---

STRUGGLE: strive to achieve or attain something in the face of difficulty or resistance

As many of you know, especially those of you who have had children, bed time can be an adventure.  In our house, it seems that there is a sudden surge in energy and emotion once the words, "bed time" leave a parent's mouth.  One almost wishes that it would be possible to trick one's children into brushing their teeth and putting on pajamas without ever having thoughts of bed time enter into even the corner of their minds.  There are many times when the phrase has escaped my lips that I wish I could just catch the sound waves before they reach the children's ears because I didn't think about what kind of a reaction I would receive.  Once you let them go, however, there is no taking them back.  What happens next is coming whether you like it or not.

Their reactions are very indicative of how our own hearts can react, too.  We might attempt to be more polished in our responses, but when our wants and desires are threatened, when someone challenges our comfort or quietude or peace, we react quite the same way that children do when they hear the words "bed time."  We come home from a long day at work, ready to kick our feet up with a glass of iced tea, a book or remote in our hand, when suddenly it begins.  What starts as a distant echo of voices becomes clearer as the running feet bring the whispering voices to shouts.  Suddenly all your hopes of some time of peace and quiet are shattered with the earsplitting cries of children fighting over a toy.  You see your perfect plan falling to pieces.  In an instant, there are now two startled children with tears welling in their eyes from your reaction to their dashing of your hopes.  

In a moment such as this, someone has threatened an idol in our lives, in this case, peace and quiet.  How we react when the idols of our lives are threatened shows the state of our hearts.  We can either respond as the children who hear the words, "bed time," or we can seek to turn it into a moment of sanctification.  We can turn to Jesus and ask for his help to make him, and not this idol, the center of our lives so that when the fighting children enter the room we can respond constructively and not in the heat of the moment.  It would not only be a time of growth for ourselves, but we would also have a stronger relationship with our children because we would not be sinning against them in the process.

What causes you to react like children at bed time?  How can you turn those idols over the Christ?

