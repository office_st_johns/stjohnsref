---
title: "Corporate Worship: Why do we come to church?"
date: 2015-09-30
author: Rev. Crawford
description : "Corporate Worship"
---

Why do we come to church?

While we have all kinds of experiences at church (for example, fellowship with other believers, singing), the main focus of our corporate worship should focus on our coming together to glorify God and to enjoy him. Everything else that we do on Sunday mornings is contingent upon this truth.  Ee do this through our song, prayer, preaching, etc., but we have to see them as ways in which we glorify God and enjoy him, not just ourselves.  To put it another way, what would fellowship mean apart from Christ?  What would prayer mean if we didn’t have God in the picture?  We need to be sure that our primary reason for coming is to glorify and enjoy God and then recognize everything that we do during corporate worship as an exercise of this truth.

The Reality of God’s Presence (Davis, Worship and the Reality of God, see below)

In our society, we have all kinds of spheres that compete for the focus of our lives.  For some, it’s the virtual world (whether Facebook or video games or online simulators, MMORPG), for others it’s the shopping world, still others the world of the NEWS, and still for others, books.  When we spend so much time engaging with them, these spheres can begin to seem more real than God, who is unseen.  We then come to worship without the realization that God’s presence is even more real than the shopping mall, the world of Facebook, or television.  The most revolutionary change for our corporate worship will be the recapturing of the reality of God as he meets with us when we gather in His name.

Biblical Theology: “God among among us”

We can trace the theme of “God among us” through the Scriptures.  We see how God’s desire from the very point of creation was to be among it (and us).  Mankind, however, fell from God’s presence in their sin, and the whole of biblical history portrays the story of how God went about restoring this relationship.  We can see it in the covenant with Abraham, as God sought to bring about a blessing not just to him but to the families of the earth (Gen. 12:1-3; 15:1-21).  We can see it when God meets his people’s need and delivers them from Egypt (Ex. 1:1-13:16).  We see his presence in the Shekinah glory cloud (“caused to dwell among;” Ex. 13, 15; Numbers 10:33-34; Deut. 31:14-15).  We see his masked glory as he descends onto Mount Sinai (Ex. 19), always with the intention that “I will be their God, and they will be my people” (Ex. 6:6-8).  We can examine the presence of God mediated in the temple and sacrificial system, his filling the temple (1 Kings 8:10-11), and his leaving the temple in his judgment (Ezek. 10).  We see the glory return in the person of Jesus Christ (John 1:14), no longer bound to a building.  We see Christ’s promises wherever two or three are gathered in his name, he will be with them (Matt. 18:20).  He has also promised that he would be with his people always, to the very end of the age (Matt. 28:18-20).  This is fulfilled as we enter into a new era when the Holy Spirit descends upon the disciples and fills them on the day of Pentecost (Acts 2).    Christ is now present, as promised, through his Holy Spirit (2 Cor 6:16), and then we see the culmination in the book of Revelation: “And I heard a loud voice from the throne saying: "Look! The residence of God is among human beings. He will live among them, and they will be his people, and God himself will be with them” (Rev. 21:3). 

 

Davis, John Jefferson, Worship and the Reality of God, (Downer’s Grove: Intervarsity Press, 2010).  I am indebted to this author for these points and the basic outline of this theme of Biblical theology.  The author also develops these ideas more fully than I ever could in a post or class.  Highly recommended.

