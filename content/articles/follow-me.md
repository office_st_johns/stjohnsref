---
title: "Follow Me"
date: 2015-08-19
author: Rev. Crawford
description : "Follow Me"
---

Follow: go after (someone) in order to observe or monitor; treat as a teacher or guide

Luke 5:27-28: After this, Jesus went out and saw a tax collector by the name of Levi sitting at his tax booth. "Follow me," Jesus said to him, and Levi got up, left everything and followed him.

Whenever a decision has to be made in our house, there always seems to be some reluctance.
"What time should we leave tomorrow?"
"I don't know...it's up to you."
"Well, I could probably be ready by 9:00."
"We've got to get the kids ready, too."
"So 9:30?"
"I don't know."
"Well, I'll plan to be ready by then and we can decide later."
"Sounds good."

Exchanges like that take place far too often.  What to have for dinner, what to do, when to go to bed, etc.  We don't have a lot of decisiveness.  If someone has a question, we often have to move into a parliamentary discussion in which we decide to table the discussion until a later meeting.  It's one of our faults.  

So you can imagine how, growing up, I was always amazed passages like these.  Jesus has deliberately set out and observed the people around him.  He selects certain people that will become his disciples, singles them out, and issues the call.  Every time he declares, "Follow me," there is an immediate decision.  Imagine the scene.  Jesus walks into the room, points to you, and says, "follow me."  I can sense a whirl of emotions in that moment, and Carvaggio's painting, "The Calling of St. Matthew," captures a lot of the probable emotions.  In the painting, Matthew's finger is pointing to himself with a "me?" question on his face.  His other hand is on a pile of money.  It's a moment of truth.  Will he hold onto the world or will he let go and follow Jesus?

The brevity of the scene in Luke strikes on all that matters: Levi left everything and followed him.  It's the same decision that we have to make today.  Are there certain aspects of this world which we don't want to release?  Are we holding onto our virtual life or our comfort in our money or the power we have in our positions?  Are we resisting the call to follow when we know what truly matters?

